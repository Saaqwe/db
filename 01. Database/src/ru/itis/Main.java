package ru.itis;

import ru.itis.jdbc.SimpleDataSource;
import ru.itis.models.Mentor;
import ru.itis.models.Student;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcImpl;

import java.sql.*;
import java.util.ArrayList;

public class Main {

    private static final String URL = "jdbc:postgresql://localhost:5432/lab_test";
    private static final String USER = "postgres";
    private static final String PASSWORD = "tim9162001";


    public static void main(String[] args) throws SQLException {
        Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(connection);

        ArrayList<Mentor> testAr = new ArrayList<>();

        Student testSt = new Student((long) 1,"Test1", "Test2", 20, 922, null);
        Student testStUpd = new Student((long) 1,"Test45", "Test77", 22, 922, null);
        //Mentor testMen = new Mentor((long) 26,"TestMent", "Test2", (long) 2, testSt);
        Mentor testMenUpd = new Mentor((long) 27,"TestMent45", "Test77", (long) 3, testSt);

        //testAr.add(testMen);
        testAr.add(testMenUpd);
        testStUpd.setMentors(testAr);
        testMenUpd.setStudent(testStUpd);
        //studentsRepository.save(testSt);
        studentsRepository.update(testStUpd);
        connection.close();


        System.out.println("hii");
    }
}
