package ru.itis.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 10.07.2020
 * 01. Database
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Mentor {

    public Mentor(Long id, String firstName, String lastName, Student student) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.student = student;
    }
    public Mentor(Long id, String firstName, String lastName, Long sub_id, Student student ) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.student = student;
        this.sub_id = sub_id;
    }
    public Mentor(String firstName, String lastName, Long sub_id, Student student ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.student = student;
        this.sub_id = sub_id;
    }

    @Override
    public String toString() {
        return "Mentor{" +
                "id=" + id +
                ", firstName='" + firstName.trim() + '\'' +
                ", lastName='" + lastName.trim() + '\'' +
                ", student=" + student +
                '}';
    }

    public  static Mentor mentorresult(ResultSet result) throws SQLException {
        return new Mentor(result.getLong("id"), result.getString("m_first_name"),
                result.getString("m_last_name"), null);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    private Long id;
    private String firstName;
    private String lastName;
    private Student student;
    private Long sub_id;

    public Long getSub_id() {
        return sub_id;
    }

    public void setSub_id(Long sub_id) {
        this.sub_id = sub_id;
    }
}
