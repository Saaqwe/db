package ru.itis.repositories;

import ru.itis.models.Mentor;
import ru.itis.models.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 10.07.2020
 * 01. Database
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class StudentsRepositoryJdbcImpl implements StudentsRepository {

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from student where id = ";

    private Connection connection;

    public StudentsRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }



    @Override
    public List<Student> findAllByAge(int ageStudent) {
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        ArrayList<Student> students = new ArrayList<Student>();
        ArrayList<Mentor> mentors = new ArrayList<>();
        String preFirstName = "";
        String preLastName = "";
        try {
            preparedStatement = connection.prepareStatement("select s.first_name, s.last_name, s.age, s.group_number,  " +
                    "m.first_name as m_first_name, m.last_name as m_last_name, m.id from student   s\n" +
                    "left join mentor m on s.id = m.student_id  where age = ? order by s.first_name, s.last_name");
            preparedStatement.setInt(1, ageStudent);
            result = preparedStatement.executeQuery();
            int k  = 0;
            while (result.next()) {
                String forTest = result.getString("first_name");
                String forTest2 = result.getString("last_name");
                if(!preFirstName.equals(forTest) && !preLastName.equals(forTest2)){
                    ArrayList<Mentor> studentsMentors = new ArrayList<>();
                    studentsMentors.add(Mentor.mentorresult(result));
                    students.add(k, new Student(
                            result.getLong("id"),
                            result.getString("first_name"), result.getString("last_name"),
                            result.getInt("age"), result.getInt("group_number"), studentsMentors
                    ));
                    k++;
                }
                else {
                    students.get(k-1).getMentors().add(Mentor.mentorresult(result));

                }
                preFirstName = result.getString("first_name");
                preLastName = result.getString("last_name");
            }
            return students;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }



    }

    // Необходимо вытащить список всех студентов, при этом у каждого студента должен быть проставлен список менторов
    // у менторов в свою очередь ничего проставлять (кроме имени, фамилии, id не надо)
    // student1(id, firstName, ..., mentors = [{id, firstName, lastName, null}, {}, ), student2, student3
    // все сделать одним запросом
    @Override
    public List<Student> findAll() {
        Statement statement = null;
        ResultSet result = null;
        ArrayList<Student> students = new ArrayList<Student>();
        ArrayList<Mentor> mentors = new ArrayList<>();
        String preFirstName = "";
        String preLastName = "";
        try {
            statement = connection.createStatement();
            result = statement.executeQuery("select s.first_name, s.last_name, s.age, s.group_number,  m.first_name as m_first_name, m.last_name as m_last_name, m.id from student s\n" +
                  "left join mentor m on s.id = m.student_id where age = 18 order by s.first_name, s.last_name");
            int k  = 0;
            while (result.next()) {
                String forTest = result.getString("first_name");
                String forTest2 = result.getString("last_name");
                if(!preFirstName.equals(forTest) && !preLastName.equals(forTest2)){
                    ArrayList<Mentor> studentsMentors = new ArrayList<>();
                    studentsMentors.add(Mentor.mentorresult(result));
                    students.add(k, new Student(
                            result.getLong("id"),
                            result.getString("first_name"), result.getString("last_name"),
                            result.getInt("age"), result.getInt("group_number"), studentsMentors
                    ));
                k++;
                }
                else {
                    students.get(k-1).getMentors().add(Mentor.mentorresult(result));
                }
                preFirstName = result.getString("first_name");
                preLastName = result.getString("last_name");
            }
            return students;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }


    }

    @Override
    public Student findById(Long id) {
        Statement statement = null;
        ResultSet result = null;

        try {
            statement = connection.createStatement();
            result = statement.executeQuery(SQL_SELECT_BY_ID + id);
            if (result.next()) {
                return new Student(
                        result.getLong("id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getInt("age"),
                        result.getInt("group_number")
                );
            } else return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    // просто вызывается insert для сущности
    // student = Student(null, 'Марсель', 'Сидиков', 26, 915)
    // studentsRepository.save(student);
    // // student = Student(3, 'Марсель', 'Сидиков', 26, 915)
    @Override
    public void save(Student entity) {
        PreparedStatement preparedStatement = null;
        ArrayList<Student> students = new ArrayList<Student>();
        ArrayList<Mentor> mentors = new ArrayList<>();
        ResultSet result = null;

        try{
            preparedStatement = connection.prepareStatement("insert into student values (default , ?, ?, ?, ?)");
            preparedStatement.setString(1,entity.getFirstName());
            preparedStatement.setString(2,entity.getLastName());
            preparedStatement.setInt(3, entity.getAge());
            preparedStatement.setInt(4, entity.getGroupNumber());
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement("select max(id) as st_id from student");
            result = preparedStatement.executeQuery();
            long st_id = 0;
            if(result.next()) {
                st_id = result.getLong("st_id");
            }


            mentors = (ArrayList<Mentor>) entity.getMentors();
            for(Mentor m : mentors) {
                preparedStatement = connection.prepareStatement("insert into mentor values (default , ?, ?, ?, ?)");
                preparedStatement.setString(1, m.getFirstName());
                preparedStatement.setString(2, m.getLastName());
                preparedStatement.setLong(3, m.getSub_id());
                preparedStatement.setLong(4, st_id);
            }
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

        finally {
            if(preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    // для сущности, у которой задан id выполнить обновление всех полей

    // student = Student(3, 'Марсель', 'Сидиков', 26, 915)
    // student.setFirstName("Игорь")
    // student.setLastName(null);
    // studentsRepository.update(student);
    // (3, 'Игорь', null, 26, 915)

    @Override
    public void update(Student entity) {
        PreparedStatement preparedStatement = null;
        ArrayList<Student> students = new ArrayList<Student>();
        ArrayList<Mentor> mentors = new ArrayList<>();
        ResultSet result = null;
        ResultSet result2 = null;
        String f_name = entity.getFirstName();
        String l_name  = entity.getLastName();
        int age = entity.getAge();
        int g_num = entity.getGroupNumber();
        long id = entity.getId();
        try{
            preparedStatement = connection.prepareStatement("update student set first_name= ?, last_name = ?, age = ?, group_number = ? where id = ?" );
            preparedStatement.setString(1, f_name);
            preparedStatement.setString(2, l_name);
            preparedStatement.setInt(3, age);
            preparedStatement.setInt(4,g_num);
            preparedStatement.setLong(5, id);
            preparedStatement.executeUpdate();
            ArrayList<Mentor> updatedMentors = (ArrayList<Mentor>) entity.getMentors();
            ArrayList<Mentor> currentMentors = new ArrayList<>();
            for(Mentor m : updatedMentors) {
                preparedStatement = connection.prepareStatement("select * from mentor where id = ?");
                preparedStatement.setLong(1, m.getId());
                result = preparedStatement.executeQuery();
                if(!result.next()) {
                    preparedStatement = connection.prepareStatement("insert into mentor values (?, ?, ?, ?, ?)");
                    preparedStatement.setLong(1,m.getId());
                    preparedStatement.setString(2, m.getFirstName());
                    preparedStatement.setString(3, m.getLastName());
                    preparedStatement.setLong(4, m.getSub_id());
                    preparedStatement.setLong(5,entity.getId());
                    preparedStatement.executeUpdate();
                }
            }

            preparedStatement = connection.prepareStatement("select  * from  mentor where  student_id = ?");
            preparedStatement.setLong(1, entity.getId());
            result = preparedStatement.executeQuery();
            while (result.next()) {
                id = result.getLong("id");
                for(int i = 0; i < updatedMentors.size(); i++) {
                    if(updatedMentors.get(i).getId() != id) {
                        preparedStatement = connection.prepareStatement("delete * from mentor where id = ?");
                        preparedStatement.setLong(1, id);
                        preparedStatement.executeUpdate();
                    }
                }
            }
        } catch (SQLException e) {
            throw  new IllegalArgumentException(e);
        }
        finally {
            if(preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }
}
