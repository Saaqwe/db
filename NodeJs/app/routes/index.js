const profile = require('./profile');
const resp = require('./resp');
module.exports = function(app) {
    profile(app);
    resp(app);
};
