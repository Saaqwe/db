bodyParser = require('body-parser').json();
const fs = require("fs");
module.exports = function (app) {
    app.get('/profile', (request, response) => {
        let result = [{
            "title": "Profile",
            "content": "В настоящий момент являюсь студентом, но за последние 2 лет приобрел достаточно опты на pet проектах, имею опыт разработки парссеров и " +
                "простоых лендингов, хочу дальше развиваться в данномнаправление."
        },
            {
                "title": "My achievement",
                "content": "Закончил 11 клссов и почти первый курс,Разработал несолько работающих программ," +
                    "Выиграл супер олимпиаду"
            },
            {
                "title": "My experience",
                "content": "11 лет школы,1 год университета,2 годы разработки pet проектов"
            }];
        response.setHeader("Content-Type", "application/json");
        response.send(JSON.stringify(result));
    });

    app.post('/profile', bodyParser, (request, response) => {
        let body = request.body;
        console.log(body["title"]);
        console.log(body["content"]);
        fs.writeFileSync("postResult.txt", body["title"] + "," + body["content"]);
        let responseBody = {
            "name" : body["name"],
            "content" : body["content"]
        };
        response.setHeader("Content-Type", "application/json");
        response.send(JSON.stringify(responseBody));
    });


};
