bodyParser = require('body-parser').json();
const fs = require("fs");
module.exports = function (app) {
    app.get('/resp', (request, response) => {
        let fileContent = fs.readFileSync("postResult.txt", "utf8");
        let result = [
        ];
        let fileDataArray = fileContent.split(".");
        for(let i = 0; i < fileDataArray.length; i++) {
            let cur = fileDataArray[i].split(",");
            result.push({
               "fio": cur[0],
               "email" : cur[1]
            });
        }
        response.setHeader("Content-Type", "application/json");
        response.send(JSON.stringify(result));
    });

    app.post('/resp', bodyParser, (request, response) => {
        let body = request.body;
        console.log(body["fio"]);
        console.log(body["email"]);
        fs.appendFileSync("postResult.txt", body["fio"] + "," + body["email"] + ".");
        let responseBody = {
            "fio" : body["fio"],
            "email" : body["email"]
        };
        response.setHeader("Content-Type", "application/json");
        response.send(JSON.stringify(responseBody));
    });

};
