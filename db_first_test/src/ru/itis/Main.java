package ru.itis;

import java.sql.*;
import java.io.*;
public class Main {

    private static  final String URL = "jdbc:postgresql://localhost:5432/db_in_java";
    private static final String USER = "postgres";
    private static final String PASSWORD = "tim9162001";


    public static void main(String[] args) throws SQLException{
        SimpleDataSource dataSource = new SimpleDataSource();
        Connection connection = dataSource.openConnection(URL, USER, PASSWORD);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from users");
        while(resultSet.next()) {
            System.out.println("ID " + resultSet.getInt("id"));
            System.out.println("login " + resultSet.getString("login"));
        }
        resultSet.close();

        // resultSet =  statement.executeQuery("select * " +
        //          "from users u " +
        //         "         inner join active_users act on u.id = act.id;");
        connection.close();
    }
}
